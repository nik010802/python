from string import whitespace

white = '███'
black = '   '
a = [['b', 'w', 'b', 'w', 'b', 'w', 'b', 'w'],
     ['w', 'b', 'w', 'b', 'w', 'b', 'w', 'b'],
     ['b', 'w', 'b', 'w', 'b', 'w', 'b', 'w'],
     ['w', 'b', 'w', 'b', 'w', 'b', 'w', 'b'],
     ['b', 'w', 'b', 'w', 'b', 'w', 'b', 'w'],
     ['w', 'b', 'w', 'b', 'w', 'b', 'w', 'b'],
     ['b', 'w', 'b', 'w', 'b', 'w', 'b', 'w'],
     ['w', 'b', 'w', 'b', 'w', 'b', 'w', 'b']]

for i in range(len(a)):
    print(' ')
    for j in range(len(a)):
        if a[i][j]=='w':
            print(white, end='')
        else:
            print(black, end='')