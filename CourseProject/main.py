import csv
from posixpath import abspath
import pandas as pd
import numpy as np
import sqlite3
import os
import zipfile
import shutil

# from parso import parse
# from prettytable import from_db_cursor

path = 'C:/Users/fn111/Desktop/Политех/4Семестр/Python/master/python/CourseProject'
path2 = 'C:/Users/fn111/Desktop/Политех'
path_to_sql = 'C:/Users/fn111/Desktop/Политех/4Семестр/Python/master/python/CourseProject/images.db'
path3 = input('Введите путь к папке:\n').strip(' ')
a = list()

str1 = ''
ext = ['AVIF', 'WEBP', 'SVG', 'RAW', 'AI', 'CDR', 'PDF', 'PNG', 'JPG', 'JPEG']


def temp_photo_dir(parser):
    cnt = 0
    temp_list = list()
    temp_path = path3+'/temp'
    os.mkdir(temp_path)
    for i in range(len(parser)):
        temp_list.append(shutil.copy(
            parser[i], temp_path+f'/{cnt}_'+parser[i][parser[i].rfind('/')+1:]))
        cnt += 1
    zipF = zipfile.ZipFile('images.zip', 'w', zipfile.ZIP_DEFLATED)
    for i in range(len(temp_list)//2):
        zipF.write(temp_list[i])
    shutil.rmtree(temp_path)

def sql_csv(sql_path):
    sqlite_connection = sqlite3.connect(sql_path)
    pd.read_sql("SELECT * FROM 'image'",
                sqlite_connection).to_csv('images.csv', sep='\t')
                
def parser(string):
    os.chdir(string)
    for dirs in os.walk(os.curdir):
        str1 = ''
        str1 = abspath(dirs[0])
        str1 = str1.replace('.\\', '')
        str1 = str1.replace('\\', "/")
        for i in range(len(dirs[2])):
            if(dirs[2][i][dirs[2][i].rfind('.')+1:].upper() in ext):
                str2 = str1 + '/' + dirs[2][i]
                a.append(str2)
    return(a)


def convert_to_binary_data(filename):
    # Преобразование данных в двоичный формат
    with open(filename, 'rb') as file:
        blob_data = file.read()
    return blob_data





def main_func(lst):
    try:
        cnt = 0
        mode = 1

        sqlite_connection = sqlite3.connect('images.db')
        cursor = sqlite_connection.cursor()
        sqlite_create_table_query = """CREATE TABLE IF NOT EXISTS image
                                  (file_path TEXT, photo BLOB, st_atime INT, st_ino INT, st_mode INT, st_dev INT, st_nlink INT, st_uid INT, st_gid INT, st_size INT, st_ctime INT, st_mtime INT)"""
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_insert_blob_query = """INSERT INTO image
                                  (photo,file_path, st_atime, st_ino, st_mode, st_dev, st_nlink, st_uid, st_gid, st_size, st_ctime, st_mtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

        for i in range(len(lst)):
            str1 = lst[i].replace(':', '')
            cursor.execute(
                f"SELECT EXISTS(SELECT 1 FROM 'image' WHERE file_path = '{str1}' LIMIT 1)")
            a = cursor.fetchone()
            if(a[0] == 0):
                sqlite_connection.commit()
                photo = convert_to_binary_data(lst[i])
                statinfo = os.stat(lst[i])
                # Преобразование данных в формат кортежа
                data_tuple = (photo, str1, statinfo.st_atime, statinfo.st_ino,
                              statinfo.st_mode, statinfo.st_dev, statinfo.st_nlink, statinfo.st_uid, statinfo.st_gid, statinfo.st_size, statinfo.st_ctime, statinfo.st_mtime)
                cursor.execute(sqlite_insert_blob_query, data_tuple)
                sqlite_connection.commit()
                cnt += 1
                if(cnt % 10 == 0):
                    print(f'Обработано уже {cnt} изображений')
            else:
                if(mode != 1 and mode != 2):
                    sqlite_connection.commit()
                    continue
                elif(mode == 2 or input('В базе содержатся дубликаты, хотите ли вы перезаписать их?\n').upper() == 'ДА'):
                    sqlite_connection.commit()
                    cursor.execute(
                        f"DELETE FROM image WHERE file_path = '{str1}'")
                    sqlite_connection.commit()
                    photo = convert_to_binary_data(lst[i])
                    statinfo = os.stat(lst[i])
                    # Преобразование данных в формат кортежа
                    data_tuple = (photo, str1, statinfo.st_atime, statinfo.st_ino,
                                  statinfo.st_mode, statinfo.st_dev, statinfo.st_nlink, statinfo.st_uid, statinfo.st_gid, statinfo.st_size, statinfo.st_ctime, statinfo.st_mtime)
                    cursor.execute(sqlite_insert_blob_query, data_tuple)
                    sqlite_connection.commit()
                    cnt += 1
                    mode = 2
                else:
                    mode = 0

        cursor.close()

    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        print(f'Всего добавлено в БД {cnt} изображений')
        if(input('Хотите ли вы создать архив со всеми изображениями?\n').upper() == 'ДА'):
            sqlite_connection.commit()
            temp_photo_dir(parser(path3))
        if(input('Хотите ли вы создать csv файл со всей информацией?\n').upper() == 'ДА'):
            sqlite_connection.commit()
            sql_csv(path3+'/images.db')
        if sqlite_connection:
            sqlite_connection.close()
            print(f'Путь к базе данных:{path3}')


main_func(parser(path3))
