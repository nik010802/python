from math import ceil
from random import choice

wall = "███"
soft = "░░░"

while True:
    w = int(input("Ширина? "))
    h = int(input("Высота? "))

    if w % 4 == 0:
        w = w
    else:
        # Ширина должна быть кратна 4
        w = w + 4 - w % 4

    if h == 1:
        h = h + 2
    elif h % 2 != 0:
        h = h
    else:
        # Высота должна быть нечетной
        h = h + 1
    for i in range(h):
        print('')
        for j in range(w+1):
            # Рисуем стены
            if i == 0 or i == (h-1):
                print(wall, end='')
            elif j == 0 or j == w:
                print(wall, end='')
            elif j % 2 == 0 and i % 2 == 0:
                print(wall, end='')
            # Создаем места для игроков
            elif (i == 1 and (j == 1 or j == 2 or j == w-1 or j == w-2))\
                    or (i == 2 and (j == 1 or j == w-1))\
                    or (i == h-3 and ((j == 1 or j == w-1)))\
                    or (i == h-2 and (j == 1 or j == 2 or j == w-1 or j == w-2)):
                print('   ', end='')
            # Рисуем мягкие стены
            else:
                if choice([True, False]):
                    print(soft, end='')
                else:
                    print('   ', end='')
    print('')
    if input("Выйти?(q)")=='q':
        break   