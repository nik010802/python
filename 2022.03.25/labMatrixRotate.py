from operator import le
from pickle import TRUE
import random
import numpy as np

print("Введите количество строк матрицы: ")
x = int(input())
print("Введите количество столбцов матрицы: ")
y = int(input())
array = []
for i in range(x):
    array.append([])
    for j in range(y):
        array[i].append(random.randint(10, 99))
     
for i in range(0, len(array)):
    for i2 in range(0, len(array[i])):
        print(array[i][i2], end=' ')
    print()

while(True):
    print("Введите действие над матрицей(clock/contrclock/reflect/exit):")
    a = input()
    if(a == "reflect"):
        arraycopy = array.copy()
        array.clear()
        for i in range(x):
            array.append([])
            for j in range(y):
                array[i].append(arraycopy[j][i])
        
        for i in range(0, len(array)):
            for i2 in range(0, len(array[i])):
                print(array[i][i2], end=' ')
            print()
    elif(a == 'clock'):
        array=np.rot90(array,-1)
        for i in range(0, len(array)):
            for i2 in range(0, len(array[i])):
                print(array[i][i2], end=' ')
            print()
    elif(a=="contrclock"):
        array=np.rot90(array,1)
        for i in range(0, len(array)):
            for i2 in range(0, len(array[i])):
                print(array[i][i2], end=' ')
            print()
    elif(a == 'exit'):
        break
