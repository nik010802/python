import math
import time
import random
from random import randrange


def dv(k):
    x = ''
    if(k == 0):
        x = '0'
    else:
        while k > 0:
            x = str(k % 2) + x
            k = k // 2
    return(x)


def sh(k):
    x = ''
    h = '0123456789ABCDEF'
    if(k == 0):
        x = '0'
    else:
        while k > 0:
            x = h[n % 16] + x
            k = k // 16
    return(x)


while(True):
    a = input('Выберите уровень сложности(child/easy/normal/hard):\n')
    q_count = int(input('Введите количество вопросов:'))
    game_mode = input('Введите режим игры(16/2)')

    c_count = 0
    u_count = 0

    answer = ''
    if(a == 'child'):
        if(game_mode == '2'):
            while(c_count+u_count < q_count):
                n = randrange(0, 7)
                print(dv(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1
        elif(game_mode == '16'):
            while(c_count+u_count < q_count):
                n = randrange(0, 7)
                print(sh(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1
    elif(a == 'easy'):
        if(game_mode == '2'):
            while(c_count+u_count < q_count):
                n = randrange(0, 15)
                print(dv(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1
        elif(game_mode == '16'):
            while(c_count+u_count < q_count):
                n = randrange(0, 15)
                print(sh(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1
    elif(a == 'normal'):
        if(game_mode == '2'):
            while(c_count+u_count < q_count):
                n = randrange(0, 63)
                print(dv(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1
        elif(game_mode == '16'):
            while(c_count+u_count < q_count):
                n = randrange(0, 63)
                print(sh(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1
    elif(a == 'hard'):
        if(game_mode == '2'):
            while(c_count+u_count < q_count):
                n = randrange(0, 255)
                print(dv(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1
        elif(game_mode == '16'):
            while(c_count+u_count < q_count):
                n = randrange(0, 255)
                print(sh(n))
                answer = int((input('Введите число:')))
                if(answer == n):
                    c_count += 1
                else:
                    u_count += 1

    print(
        f'\nВерных ответов: {c_count}.\nНеверных ответов {u_count}.\nВсего вопросов: {q_count}', sep=' ')
    restart = input('\nПовторить игру?(y/n)')
    if(restart == 'y'):
        continue
    else:
        break
